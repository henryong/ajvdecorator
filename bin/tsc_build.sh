#!/bin/sh

# Remove previous builds
rm -rf dist &
rm -rf minified &
wait

# -----------------------------------------------------------------------------
# Build treeshakeable version
# -----------------------------------------------------------------------------
# Build ONLY the type declaration files WITH comments intact
tsc -p tsconfig.spec.json --emitDeclarationOnly --declaration --removeComments false &
# Transpile the TS files into JS files without re-building type-declarations
tsc -p tsconfig.spec.json --removeComments &
wait

# -----------------------------------------------------------------------------
# Build bundled version
# -----------------------------------------------------------------------------
pnpx rollup --config rollup.config.ts

# -----------------------------------------------------------------------------
# Prettify type definitions
# -----------------------------------------------------------------------------
find dist -type f -name "*.d.ts" | pnpx prettier --write --loglevel=silent --config .prettierrc.yml dist/. &
find minified -type f -name "*.d.ts" | pnpx prettier --write --loglevel=silent --config .prettierrc.yml minified/. &
wait

# -----------------------------------------------------------------------------
# Further minify bundled file (TO BE LAST STEP)
# -----------------------------------------------------------------------------
pnpx esbuild --format=cjs esbuild.config.ts | node
