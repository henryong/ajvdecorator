#!/bin/sh

pnpx pinst --disable
npm --no-git-tag-version version patch
git add .
git commit -m "chore(release): bump version"
