import {nodeResolve} from '@rollup/plugin-node-resolve';
import commonjs from '@rollup/plugin-commonjs';
import {terser} from 'rollup-plugin-terser';
import json from '@rollup/plugin-json';
import sizes from 'rollup-plugin-sizes';
import dts from 'rollup-plugin-dts';
// import {RollupOptions} from 'rollup';

export default [
  {
    input: 'dist/index',
    output: [
      {
        name: 'AjvDecorators',
        format: 'cjs', // UMD - both , CJS - NodeJs , IIFE - browser
        file: 'minified/index.min.cjs.js',
        sourcemap: !true,
        compact: true,
        validate: true,
        exports: 'auto'
      }
    ],
    external: ['esbuild'],
    plugins: [
      json(),
      commonjs(),
      nodeResolve(),
      terser({
        ecma: 2020,
        compress: {
          passes: 5,
          drop_console: true
        },
        toplevel: true,
        keep_classnames: true,
        keep_fnames: true,
        numWorkers: 4,
        format: {comments: false}
      }),
      sizes()
    ]
  },
  {
    input: './dist/index.d.ts',
    output: [{file: 'minified/index.min.cjs.d.ts', format: 'es'}],
    plugins: [dts(), sizes()]
  }
];
