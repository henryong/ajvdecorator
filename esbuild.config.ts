#!/usr/bin/env node

import {build as esbuild} from 'esbuild';
import {esbuildPluginNodeExternals} from 'esbuild-plugin-node-externals';

const outDir = './minified/';
const filePath = outDir + 'index.min.cjs.js';

try {
  esbuild({
    entryPoints: [filePath],
    outdir: outDir,
    platform: 'node',
    target: ['node16'],
    minify: true,
    minifySyntax: true,
    minifyIdentifiers: true,
    minifyWhitespace: true,
    bundle: !true,
    keepNames: !true,
    treeShaking: true,
    sourcemap: !true,
    allowOverwrite: true,
    plugins: [esbuildPluginNodeExternals()]
  }).catch(() => process.exit(1));
} catch (error) {
  throw new Error(`Failed to write file ${filePath}.\n${error}`);
}
