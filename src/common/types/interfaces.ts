import {Options} from 'ajv';
import {AjvErrorObject} from '.';

export interface IValidationResults {
  success: boolean;
  errors?: AjvErrorObject;
}
export interface IValidatorOptions extends Options {
  generateCode?: boolean;
  outDir?: string;
}
export interface IObject {
  [k: string]: any;
}
export interface IOptions {
  message?: string;
}
export interface IFactoryParams {
  func: Function;
  options?: IOptions;
  expected?: any;
}
