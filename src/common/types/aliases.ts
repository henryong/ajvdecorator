import {ErrorObject} from 'ajv';

export type AjvErrorObject = ErrorObject<string, Record<string, any>, unknown>[] | null;
export type Enums = {[s: string]: any} | ArrayLike<any>;
export type Constructor = new (...args: any[]) => {} | Function;
