import {JsonPointer} from 'json-ptr';
import {ValidationUtility} from '../validation/ValidationUtility';
import {REFLECT} from '../constants/reflect-api';
import {IFactoryParams, Constructor, IObject} from '../common/types';

export function decorateProperty({func, expected, options}: IFactoryParams): PropertyDecorator {
  return function (target: Object, key: string | symbol): void {
    const ctor = target.constructor as Constructor;
    const propertyKey = key as string;

    switch (func.name) {
      case '_Required':
        const _isRequiredFunc: string = func.call(null, propertyKey);
        ValidationUtility.setRequired(ctor, _isRequiredFunc);
        break;
      case '_DependentRequired':
        const _isDependentRequiredFunc: IObject = func.apply(null, [propertyKey, expected]);
        ValidationUtility.setRequiredDependencies(ctor, _isDependentRequiredFunc);
        break;
      default:
        const functionCall: IObject = func.apply(null, [propertyKey, expected]);
        const targetType = JsonPointer.get(functionCall, `/${propertyKey}/type`);
        const validationObject = JsonPointer.get(functionCall, `/${propertyKey}`) as Object;
        const targetkeyword = Object.keys(validationObject).filter(key => !!key && key !== 'type')[0];
        const reflectType = Reflect.getMetadata(REFLECT.TYPE, target, propertyKey).name;

        if (targetType && targetType !== reflectType.toLowerCase()) {
          const errorMessage = `Property "${propertyKey}" is of type ${targetType} but decalred as ${reflectType}`;
          throw new Error(errorMessage);
        }

        ValidationUtility.setProperties(ctor, functionCall);

        if (options?.message && targetkeyword) {
          const propertyPath = `/properties/${propertyKey}`;
          const errornousTarget = `/errorMessage/${targetkeyword}`;
          const errorMessagePath = propertyPath + errornousTarget;

          ValidationUtility.setMessage(ctor, errorMessagePath, options.message);
        }
        break;
    }
  };
}
