import {PRIMITIVES} from '../constants';
import type {PRIMITIVES as Primitive} from '../constants';

export function GetValueType(value: any): Primitive {
  if (value === null) return PRIMITIVES.NULL;
  if (value === undefined) return PRIMITIVES.UNDEFINED;
  if (Array.isArray(value)) return PRIMITIVES.ARRAY;
  if (typeof value === 'string') return PRIMITIVES.STRING;
  if (typeof value === 'number') return PRIMITIVES.NUMBER;
  if (typeof value === 'boolean') return PRIMITIVES.BOOLEAN;
  if (typeof value === 'object') return PRIMITIVES.OBJECT;
  if (typeof value === 'function') return PRIMITIVES.FUNCTION;
  if (typeof value === 'symbol') return PRIMITIVES.SYMBOL;
  if (value instanceof Date) return PRIMITIVES.DATE;
  if (value instanceof RegExp) return PRIMITIVES.REGEXP;
  if (value instanceof Map) return PRIMITIVES.MAP;
  if (value instanceof Set) return PRIMITIVES.SET;
  if (value instanceof WeakMap) return PRIMITIVES.WEAKMAP;
  if (value instanceof WeakSet) return PRIMITIVES.WEAKSET;
  if (value instanceof Promise) return PRIMITIVES.PROMISE;
  if (value instanceof Int8Array) return PRIMITIVES.INT8ARRAY;
  if (value instanceof Uint8Array) return PRIMITIVES.UINT8ARRAY;
  if (value instanceof Uint8ClampedArray) return PRIMITIVES.UINT8CLAMPEDARRAY;
  if (value instanceof Int16Array) return PRIMITIVES.INT16ARRAY;
  if (value instanceof Uint16Array) return PRIMITIVES.UINT16ARRAY;
  if (value instanceof Int32Array) return PRIMITIVES.INT32ARRAY;
  if (value instanceof Uint32Array) return PRIMITIVES.UINT32ARRAY;
  if (value instanceof Float32Array) return PRIMITIVES.FLOAT32ARRAY;
  return PRIMITIVES.ANY;
}
