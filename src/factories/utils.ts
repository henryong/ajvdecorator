import {PRIMITIVES} from '../constants';
import type {PRIMITIVES as Primitive} from '../constants';

export function IsValueOfType(primitive: Primitive, value: any): boolean {
  switch (primitive) {
    case PRIMITIVES.ANY:
      return true;
    case PRIMITIVES.STRING:
      return typeof value === 'string';
    case PRIMITIVES.NUMBER:
      return typeof value === 'number';
    case PRIMITIVES.BOOLEAN:
      return typeof value === 'boolean';
    case PRIMITIVES.OBJECT:
      return typeof value === 'object';
    case PRIMITIVES.ARRAY:
      return Array.isArray(value);
    case PRIMITIVES.FUNCTION:
      return typeof value === 'function';
    case PRIMITIVES.SYMBOL:
      return typeof value === 'symbol';
    case PRIMITIVES.NULL:
      return value === null;
    case PRIMITIVES.UNDEFINED:
      return value === undefined;
    case PRIMITIVES.DATE:
      return value instanceof Date;
    case PRIMITIVES.REGEXP:
      return value instanceof RegExp;
    case PRIMITIVES.MAP:
      return value instanceof Map;
    case PRIMITIVES.SET:
      return value instanceof Set;
    case PRIMITIVES.WEAKMAP:
      return value instanceof WeakMap;
    case PRIMITIVES.WEAKSET:
      return value instanceof WeakSet;
    case PRIMITIVES.PROMISE:
      return value instanceof Promise;
    case PRIMITIVES.INT8ARRAY:
      return value instanceof Int8Array;
    case PRIMITIVES.UINT8ARRAY:
      return value instanceof Uint8Array;
    case PRIMITIVES.UINT8CLAMPEDARRAY:
      return value instanceof Uint8ClampedArray;
    case PRIMITIVES.INT16ARRAY:
      return value instanceof Int16Array;
    case PRIMITIVES.UINT16ARRAY:
      return value instanceof Uint16Array;
    case PRIMITIVES.INT32ARRAY:
      return value instanceof Int32Array;
    case PRIMITIVES.UINT32ARRAY:
      return value instanceof Uint32Array;
    case PRIMITIVES.FLOAT32ARRAY:
      return value instanceof Float32Array;
    default:
      return false;
  }
}
