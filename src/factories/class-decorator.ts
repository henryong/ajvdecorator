import {ValidationUtility} from '../validation/ValidationUtility';
import {Constructor, IFactoryParams} from '../common/types';
import {IObject} from '../common/types';

export function decorateClass({func, expected /* , options */}: IFactoryParams): ClassDecorator {
  return <T extends Function>(ctor: T) => {
    const classCtor = ctor as unknown as Constructor;
    const classFunc: IObject = func.call(null, expected);
    ValidationUtility.setRequiredProperties(classCtor, classFunc);
  };
}
