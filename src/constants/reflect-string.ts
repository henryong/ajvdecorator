export const enum STRING {
  STRING = 'string',
  ALPHABETICAL = 'alphabetical',
  ALPHANUMERICAL = 'alphanumerical',
  LOWERCASE = 'lowercase',
  UPPERCASE = 'uppercase'
}
