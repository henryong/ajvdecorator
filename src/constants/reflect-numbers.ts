export const enum NUMBER {
  TYPE = 'number',
  INTEGER = 'integer',
  INTEGER_32 = 'int32',
  UNSIGNED_INTEGER_32 = 'uint32'
}
