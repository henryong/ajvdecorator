import {decorateProperty} from '../../factories/property-decorator';
import {PRIMITIVES} from '../../constants';
import {IOptions, Enums} from '../../common/types';

export function _Enum(key: string, enums: Enums) {
  return {[key]: {[PRIMITIVES.ENUM]: enums}};
}

export function Enum(enums: Enums, options?: IOptions) {
  const enumerables = Object.values(enums).filter(val => Number.isNaN(Number(val)) && val);
  return decorateProperty({func: _Enum, expected: enumerables, options});
}
