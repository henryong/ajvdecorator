import {decorateProperty} from '../../factories/property-decorator';
import {PRIMITIVES} from '../../constants/reflect-primitives';
import {IOptions} from '../../common/types';

export function _IsNumber(key: string) {
  return {[key]: {type: PRIMITIVES.NUMBER}};
}

export function IsNumber(key: string, options?: IOptions) {
  return decorateProperty({func: _IsNumber, expected: key, options});
}
