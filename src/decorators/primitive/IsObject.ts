import {decorateProperty} from '../../factories/property-decorator';
import {PRIMITIVES} from '../../constants/reflect-primitives';
import {IOptions} from '../../common/types';

export function _IsObject(key: string) {
  return {[key]: {type: PRIMITIVES.OBJECT}};
}

export function IsObject(key: string, options?: IOptions) {
  return decorateProperty({func: _IsObject, expected: key, options});
}
