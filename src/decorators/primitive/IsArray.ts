import {decorateProperty} from '../../factories/property-decorator';
import {PRIMITIVES} from '../../constants/reflect-primitives';
import {IOptions} from '../../common/types';

export function _IsArray(key: string) {
  return {[key]: {type: PRIMITIVES.ARRAY}};
}

export function IsArray(options?: IOptions) {
  return decorateProperty({func: _IsArray, options});
}
