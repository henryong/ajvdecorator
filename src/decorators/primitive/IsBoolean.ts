import {decorateProperty} from '../../factories/property-decorator';
import {PRIMITIVES} from '../../constants/reflect-primitives';
import {IOptions} from '../../common/types';

export function _IsBoolean(key: string) {
  return {[key]: {type: PRIMITIVES.BOOLEAN}};
}

export function IsBoolean(key: string, options?: IOptions) {
  return decorateProperty({func: _IsBoolean, expected: key, options});
}
