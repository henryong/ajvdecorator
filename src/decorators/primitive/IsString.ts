import {decorateProperty} from '../../factories/property-decorator';
import {PRIMITIVES} from '../../constants/reflect-primitives';
import {IOptions} from '../../common/types';

export function _IsString(key: string) {
  return {[key]: {type: PRIMITIVES.STRING}};
}

export function IsString(options?: IOptions) {
  return decorateProperty({func: _IsString, options});
}
