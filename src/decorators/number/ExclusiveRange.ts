import {decorateProperty} from '../../factories/property-decorator';
import {NUMBER} from '../../constants/reflect-numbers';
import {IOptions} from '../../common/types';

export function _ExclusiveRange(key: string, exclusiveRange: number[]) {
  return {[key]: {type: NUMBER.TYPE, exclusiveRange}};
}

export function ExclusiveRange(exclusiveRange: number[], options?: IOptions) {
  return decorateProperty({func: _ExclusiveRange, expected: exclusiveRange, options});
}
