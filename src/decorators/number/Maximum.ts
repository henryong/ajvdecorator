import {decorateProperty} from '../../factories/property-decorator';
import {NUMBER} from '../../constants/reflect-numbers';
import {IOptions} from '../../common/types';

export function _Maximum(key: string, maximum: number) {
  return {[key]: {type: NUMBER.TYPE, maximum}};
}

export function Maximum(maximum: number, options?: IOptions) {
  return decorateProperty({func: _Maximum, expected: maximum, options});
}
