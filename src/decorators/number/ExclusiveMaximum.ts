import {decorateProperty} from '../../factories/property-decorator';
import {NUMBER} from '../../constants/reflect-numbers';
import {IOptions} from '../../common/types';

export function _ExclusiveMaximum(key: string, exclusiveMaximum: number) {
  return {[key]: {type: NUMBER.TYPE, exclusiveMaximum}};
}

export function ExclusiveMaximum(exclusiveMaximum: number, options?: IOptions) {
  return decorateProperty({func: _ExclusiveMaximum, expected: exclusiveMaximum, options});
}
