import {decorateProperty} from '../../factories/property-decorator';
import {NUMBER} from '../../constants/reflect-numbers';
import {IOptions} from '../../common/types';

export function _ExclusiveMinimum(key: string, exclusiveMinimum: number) {
  return {[key]: {type: NUMBER.TYPE, exclusiveMinimum}};
}

export function ExclusiveMinimum(exclusiveMinimum: number, options?: IOptions) {
  return decorateProperty({func: _ExclusiveMinimum, expected: exclusiveMinimum, options});
}
