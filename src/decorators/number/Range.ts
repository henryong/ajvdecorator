import {decorateProperty} from '../../factories/property-decorator';
import {NUMBER} from '../../constants/reflect-numbers';
import {IOptions} from '../../common/types';

export function _Range(key: string, range: number[]) {
  return {[key]: {type: NUMBER.TYPE, range}};
}

export function Range(range: number[], options?: IOptions) {
  return decorateProperty({func: _Range, expected: range, options});
}
