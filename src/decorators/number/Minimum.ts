import {decorateProperty} from '../../factories/property-decorator';
import {NUMBER} from '../../constants/reflect-numbers';
import {IOptions} from '../../common/types';

export function _Minimum(key: string, minimum: number) {
  return {[key]: {type: NUMBER.TYPE, minimum}};
}

export function Minimum(minimum: number, options?: IOptions) {
  return decorateProperty({func: _Minimum, expected: minimum, options});
}
