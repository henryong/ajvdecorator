import {decorateProperty} from '../../factories/property-decorator';
import {NUMBER} from '../../constants/reflect-numbers';
import {IOptions} from '../../common/types';

export function _MultipleOf(key: string, multipleOf: number) {
  return {[key]: {type: NUMBER.TYPE, multipleOf}};
}

export function MultipleOf(multipleOf: number, options?: IOptions) {
  return decorateProperty({func: _MultipleOf, expected: multipleOf, options});
}
