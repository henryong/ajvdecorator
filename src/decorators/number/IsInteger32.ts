import {decorateProperty} from '../../factories/property-decorator';
import {FORMATS} from '../../constants/reflect-formats';
import {IOptions} from '../../common/types';
import {NUMBER} from '../../constants/reflect-numbers';

export function _IsInteger32(key: string) {
  return {[key]: {type: NUMBER.TYPE, format: FORMATS.INTEGER_32}};
}

export function IsInteger32(options?: IOptions) {
  return decorateProperty({func: _IsInteger32, options});
}
