import {decorateProperty} from '../../factories/property-decorator';
import {FORMATS} from '../../constants/reflect-formats';
import {IOptions} from '../../common/types';
import {NUMBER} from '../../constants/reflect-numbers';

export function _IsInteger64(key: string) {
  return {[key]: {type: NUMBER.TYPE, format: FORMATS.INTEGER_64}};
}

export function IsInteger64(options?: IOptions) {
  return decorateProperty({func: _IsInteger64, options});
}
