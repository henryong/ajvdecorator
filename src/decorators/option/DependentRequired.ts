import {decorateProperty} from '../../factories/property-decorator';
import {IOptions} from '../../common/types';

export function _DependentRequired(key: string, args: string[]) {
  return {[key]: args};
}

export function DependentRequired(args: string[], options?: IOptions) {
  return decorateProperty({func: _DependentRequired, expected: args, options});
}
