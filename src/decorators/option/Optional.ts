import {decorateProperty} from '../../factories/property-decorator';
import {IOptions} from '../../common/types';

export function _Optional(key: string) {
  return key;
}

/**
 * Please declare this option at the top level of ALL decorator definitions.
 */
export function Optional(options?: IOptions) {
  return decorateProperty({func: _Optional, options});
}
