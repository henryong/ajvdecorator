import {decorateProperty} from '../../factories/property-decorator';
import {IOptions} from '../../common/types';

export function _Required(key: string) {
  return key;
}

export function Required(options?: IOptions) {
  return decorateProperty({func: _Required, options});
}
