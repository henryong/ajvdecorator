import {decorateClass} from '../../factories/class-decorator';
import {IOptions} from '../../common/types';

export function _AdditionalProperties(additionalProperties: boolean = true) {
  return {type: 'object', additionalProperties};
}

export function AdditionalProperties(expected?: boolean, options?: IOptions) {
  return decorateClass({func: _AdditionalProperties, expected, options});
}
