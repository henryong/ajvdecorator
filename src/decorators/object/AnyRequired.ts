import {decorateClass} from '../../factories/class-decorator';
import {IOptions} from '../../common/types';

export function _AnyRequired(anyRequired: string[]) {
  return {type: 'object', anyRequired};
}

export function AnyRequired(expected: string[], options?: IOptions) {
  return decorateClass({func: _AnyRequired, expected, options});
}
