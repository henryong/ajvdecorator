import {decorateClass} from '../../factories/class-decorator';
import {IOptions} from '../../common/types';

export function _PatternRequired(patternRequired: string[]) {
  return {type: 'object', patternRequired};
}

export function PatternRequired(expected: string[], options?: IOptions) {
  return decorateClass({func: _PatternRequired, expected, options});
}
