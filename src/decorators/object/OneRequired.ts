import {decorateClass} from '../../factories/class-decorator';
import {IOptions} from '../../common/types';

export function _OneRequired(oneRequired: string[]) {
  return {type: 'object', oneRequired};
}

export function OneRequired(expected: string[], options?: IOptions) {
  return decorateClass({func: _OneRequired, expected, options});
}
