import {decorateClass} from '../../factories/class-decorator';
import {IOptions} from '../../common/types';

export function _AllRequired(allRequired: boolean = true) {
  return {type: 'object', allRequired};
}

export function AllRequired(options?: IOptions) {
  return decorateClass({func: _AllRequired, options});
}
