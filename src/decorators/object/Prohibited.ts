import {decorateClass} from '../../factories/class-decorator';
import {IOptions} from '../../common/types';

export function _Prohibited(prohibited: string[]) {
  return {type: 'object', prohibited};
}

export function Prohibited(expected: string[], options?: IOptions) {
  return decorateClass({func: _Prohibited, expected, options});
}
