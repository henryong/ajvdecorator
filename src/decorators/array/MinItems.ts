import {decorateProperty} from '../../factories/property-decorator';
import {IOptions} from '../../common/types';

export function _MinItems(key: string, minItems: number) {
  return {[key]: {type: 'array', minItems}};
}

export function MinItems(options?: IOptions) {
  return decorateProperty({func: _MinItems, options});
}
