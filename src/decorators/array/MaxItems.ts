import {decorateProperty} from '../../factories/property-decorator';
import {IOptions} from '../../common/types';

export function _MaxItems(key: string, maxItems: number) {
  return {[key]: {type: 'array', maxItems}};
}

export function MaxItems(options?: IOptions) {
  return decorateProperty({func: _MaxItems, options});
}
