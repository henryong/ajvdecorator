import {decorateProperty} from '../../factories/property-decorator';
import {IOptions} from '../../common/types';

export function _UniqueItems(key: string, uniqueItems: boolean) {
  return {[key]: {type: 'array', uniqueItems}};
}

export function UniqueItems(options?: IOptions) {
  return decorateProperty({func: _UniqueItems, options});
}
