import {decorateProperty} from '../../factories/property-decorator';
import {STRING} from '../../constants/reflect-string';
import {IOptions} from '../../common/types';

export function _Alphabetical(key: string) {
  return {[key]: {type: 'string', format: STRING.ALPHABETICAL}};
}

export function IsAlphabetic(options?: IOptions) {
  return decorateProperty({func: _Alphabetical, options});
}
