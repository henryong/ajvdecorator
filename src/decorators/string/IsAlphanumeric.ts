import {decorateProperty} from '../../factories/property-decorator';
import {STRING} from '../../constants/reflect-string';
import {IOptions} from '../../common/types';

export function _Alphanumerical(key: string) {
  return {[key]: {type: 'string', format: STRING.ALPHANUMERICAL}};
}

export function IsAlphanumeric(options?: IOptions) {
  return decorateProperty({func: _Alphanumerical, options});
}
