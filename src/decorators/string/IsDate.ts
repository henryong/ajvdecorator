import {decorateProperty} from '../../factories/property-decorator';
import {FORMATS} from '../../constants/reflect-formats';
import {IOptions} from '../../common/types';

export function _IsDate(key: string) {
  return {[key]: {type: 'string', format: FORMATS.DATE}};
}

export function IsDate(options?: IOptions) {
  return decorateProperty({func: _IsDate, options});
}
