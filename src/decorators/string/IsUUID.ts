import {decorateProperty} from '../../factories/property-decorator';
import {FORMATS} from '../../constants/reflect-formats';
import {IOptions} from '../../common/types';

export function _IsUUID(key: string) {
  return {[key]: {type: 'string', format: FORMATS.UUID}};
}

export function IsUUID(options?: IOptions) {
  return decorateProperty({func: _IsUUID, options});
}
