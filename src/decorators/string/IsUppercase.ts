import {decorateProperty} from '../../factories/property-decorator';
import {STRING} from '../../constants/reflect-string';
import {IOptions} from '../../common/types';

export function _IsUppercase(key: string) {
  return {[key]: {type: 'string', [STRING.UPPERCASE]: true}};
}

export function IsUppercase(options?: IOptions) {
  return decorateProperty({func: _IsUppercase, options});
}
