import {decorateProperty} from '../../factories/property-decorator';
import {FORMATS} from '../../constants/reflect-formats';
import {IOptions} from '../../common/types';

export function _IsStrongPassword(key: string) {
  return {[key]: {type: 'string', format: FORMATS.PASSWORD}};
}

export function IsStrongPassword(options?: IOptions) {
  return decorateProperty({func: _IsStrongPassword, options});
}
