import {decorateProperty} from '../../factories/property-decorator';
import {FORMATS} from '../../constants/reflect-formats';
import {IOptions} from '../../common/types';

export function _IsIPv4(key: string) {
  return {[key]: {type: 'string', format: FORMATS.IPV4}};
}

export function IsIPv4(options?: IOptions) {
  return decorateProperty({func: _IsIPv4, options});
}
