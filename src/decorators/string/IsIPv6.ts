import {decorateProperty} from '../../factories/property-decorator';
import {FORMATS} from '../../constants/reflect-formats';
import {IOptions} from '../../common/types';

export function _IsIPv6(key: string) {
  return {[key]: {type: 'string', format: FORMATS.IPV6}};
}

export function IsIPv6(options?: IOptions) {
  return decorateProperty({func: _IsIPv6, options});
}
