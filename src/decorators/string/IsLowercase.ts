import {decorateProperty} from '../../factories/property-decorator';
import {STRING} from '../../constants/reflect-string';
import {IOptions} from '../../common/types';

export function _IsLowercase(key: string) {
  return {[key]: {type: 'string', [STRING.LOWERCASE]: true}};
}

export function IsLowercase(options?: IOptions) {
  return decorateProperty({func: _IsLowercase, options});
}
