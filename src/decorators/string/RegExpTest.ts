import {decorateProperty} from '../../factories/property-decorator';
import {IOptions} from '../../common/types';

export function _RegExpTest(key: string, pattern: number) {
  try {
    new RegExp(/${pattern}/g);
  } catch (e) {
    throw new Error(`Invalid regular expression: ${pattern}`);
  }
  return {[key]: {type: 'string', pattern}};
}

export function RegExpTest(pattern: number, options?: IOptions) {
  return decorateProperty({func: _RegExpTest, expected: pattern, options});
}
