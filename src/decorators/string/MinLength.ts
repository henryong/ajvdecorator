import {decorateProperty} from '../../factories/property-decorator';
import {IOptions} from '../../common/types';

export function _MinLength(key: string, minLength: number) {
  return {[key]: {type: 'string', minLength}};
}

export function MinLength(minLength: number, options?: IOptions) {
  return decorateProperty({func: _MinLength, expected: minLength, options});
}
