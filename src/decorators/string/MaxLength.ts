import {decorateProperty} from '../../factories/property-decorator';
import {IOptions} from '../../common/types';

export function _MaxLength(key: string, maxLength: number) {
  return {[key]: {type: 'string', maxLength}};
}

export function MaxLength(maxLength: number, options?: IOptions) {
  return decorateProperty({func: _MaxLength, expected: maxLength, options});
}
