import {decorateProperty} from '../../factories/property-decorator';
import {FORMATS} from '../../constants/reflect-formats';
import {IOptions} from '../../common/types';

export function _IsEmail(key: string) {
  return {[key]: {type: 'string', format: FORMATS.EMAIL}};
}

export function IsEmail(options?: IOptions) {
  return decorateProperty({func: _IsEmail, options});
}
