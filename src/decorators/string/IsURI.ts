import {decorateProperty} from '../../factories/property-decorator';
import {FORMATS} from '../../constants/reflect-formats';
import {IOptions} from '../../common/types';

export function _IsURI(key: string) {
  return {[key]: {type: 'string', format: FORMATS.URI}};
}

export function IsURI(options?: IOptions) {
  return decorateProperty({func: _IsURI, options});
}
