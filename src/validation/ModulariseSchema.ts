import {IObject} from '../common/types';
import {Validator} from './Validator';
import {build as esbuild} from 'esbuild';
import {esbuildPluginNodeExternals} from 'esbuild-plugin-node-externals';
import standaloneCode from 'ajv/dist/standalone';
import {writeFile} from 'fs/promises';

/**
 * Generates file that contains code of validator for a given schema.
 * Minifies, treeshakes, and bundles the generated schema file for smaller build size.
 * @export
 * @class ModulariseSchema
 */
export class ModulariseSchema {
  /**
   * Private constructor prevents creation of new instance of class.
   * @memberof ModulariseSchema
   */
  private constructor() {}
  /**
   * Generates string of validator code.
   * Use this to generate non-minified schema code.
   * @static
   * @template T
   * @param {T} [dto]
   * @return {string}
   * @memberof ModulariseSchema
   */
  public static generate<T extends {}>(dto?: T): string {
    if (!Validator.ajvInstance || !Validator.options || !Validator.options.generateCode) {
      throw new Error('Validator not configured with option to generate code.');
    }

    const dtoSchemaName = (dto?: T): IObject | undefined => {
      if (dto) {
        Validator.addInstanceSchema(dto);
        const $id = dto.constructor.name;
        return {[$id]: $id};
      }
    };

    //TODO: naive solution to string replace to manipulate the output to look like {success, errors}
    return standaloneCode(Validator.ajvInstance, dtoSchemaName(dto)).replace(
      /return errors === 0;/,
      `return {success:!errors, errors: vErrors}`
    );
  }
  /**
   * Takes in generated string of validator code and writes file to file directory.
   * Minifies, treeshakes, and bundles the generated schema file for smaller build size.
   * @static
   * @template T
   * @param {T} ctor
   * @return {Promise<void>}
   * @memberof ModulariseSchema
   */
  public static async generateCode<T extends {}>(ctor: T): Promise<void> {
    if (!Validator.options?.outDir) {
      throw new Error('"options.outDir" not specified.');
    }

    const fileDir = Validator.options.outDir;
    const fileName = ctor.constructor.name;
    const filePath = `${fileDir}/${fileName}.js`;
    const validator: string = ModulariseSchema.generate(ctor);

    await writeFile(filePath, validator, {encoding: 'utf8'}).catch(error => {
      const message = `Failed to write file ${filePath}.\n${error}`;
      throw new Error(message);
    });
    await esbuild({
      entryPoints: [filePath],
      outdir: fileDir,
      platform: 'node',
      target: ['node16'],
      minify: true,
      minifySyntax: true,
      minifyIdentifiers: true,
      bundle: true,
      keepNames: true,
      treeShaking: true,
      sourcemap: !true,
      allowOverwrite: true,
      plugins: [esbuildPluginNodeExternals()]
    }).catch(() => process.exit(1));
  }
}
