import Ajv, {JSONSchemaType, ValidateFunction} from 'ajv';
import {ValidationUtility} from './ValidationUtility';

import {Constructor, IValidatorOptions, AjvErrorObject, IValidationResults} from '../common/types';
import {AjvSingleton} from './AjvSingleton';
/**
 * @export
 * @class Validator
 * @description
 * It is recommended to use a single Ajv instance for the whole application,
 * so if you use validation in more than one module, you should:
 * - require Ajv in a separate module responsible for validation
 * - compile all validators there
 * - export them to be used in multiple modules of your application
 *
 * Reasons for:
 * - 1  main technical advantage for singleton in JS/TS is lazy-loading.
 * - Singleton will be instantiated only when we first need it.
 *
 * Reasons against:
 * - If the singleton contains stateful values.
 * - If it is time-consuming.
 * - If it is memory-intensive to instantiate.
 */
export class Validator {
  static ajvInstance: Ajv;
  static options?: IValidatorOptions;
  /**
   * Private constructor prevents creation of new instance of class.
   * @memberof Validator
   */
  private constructor() {}
  /**
   * Takes in external configuration object and initialises Ajv singleton instance.
   * @static
   * @param {IValidatorOptions} [options]
   * @memberof Validator
   */
  public static init(options?: IValidatorOptions) {
    Validator.options = options;
    Validator.ajvInstance = AjvSingleton.getInstance(options);
  }
  /**
   * @static
   * @template T
   * @param {T} dto
   * @return {IValidationResults}
   * @memberof Validator
   */
  public static validate<T extends {}>(dto: T): IValidationResults {
    if (Object.keys(dto).length < 1) {
      return {
        success: false,
        errors: [{message: 'Cannot validate empty Object'}] as AjvErrorObject
      };
    }

    const dtoSchema = Validator.validationSchema(dto);
    const validate = Validator.ajvInstance.compile(dtoSchema);

    return {success: validate(dto), errors: validate.errors};
  }
  /**
   * @static
   * @template T
   * @param {T} dto
   * @return {JSONSchemaType<T>}
   * @memberof Validator
   * @description Reasons for using `<T extends {}>` due to
   * {@link https://devblogs.microsoft.com/typescript/announcing-typescript-3-5/#breaking-changes|breaking changes} in TypeScript.
   */
  public static validationSchema<T extends {}>(dto: T): JSONSchemaType<T> {
    return ValidationUtility.ajvJsonSchema.get(dto.constructor as Constructor) as JSONSchemaType<T>;
  }
  //----------------------------------------------------------------
  // The following methods are used for compiled schema validations.
  //----------------------------------------------------------------
  /**
   * @static
   * @template T
   * @param {T} dto
   * @return {void}
   * @memberof Validator
   */
  public static addInstanceSchema<T extends {}>(dto: T): void {
    const $id = dto.constructor.name;
    const dtoSchema = Validator.validationSchema(dto);
    const individualSchema = Object.assign({$id}, dtoSchema);

    Validator.ajvInstance.addSchema(individualSchema);
  }
  /**
   * @static
   * @template T
   * @param {T} dto
   * @return {ValidateFunction<T>}
   * @memberof Validator
   */
  public static getInstanceSchema<T extends {}>(dto: T): ValidateFunction<T> {
    const $id: string = dto.constructor.name;
    const dtoSchema = Validator.validationSchema(dto);

    return Validator.ajvInstance.getSchema<T>($id) ?? Validator.ajvInstance.compile<T>(dtoSchema);
  }
  /**
   * @static
   * @template T
   * @param {T} dto
   * @return {IValidationResults}
   * @memberof Validator
   * @description AOT validation
   */
  public static ajvInstanceValidation<T extends {}>(dto: T): IValidationResults {
    const dtoSchemaRef = {$ref: `${dto.constructor.name}#`};
    const success = Validator.ajvInstance.validate(dtoSchemaRef, dto);
    const errors = Validator.ajvInstance.errors;

    return {success, errors};
  }
}
