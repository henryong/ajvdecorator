import {JsonPointer} from 'json-ptr';
import {IObject, Constructor} from '../common/types';
import merge from 'lodash.merge';

const schemaKeyValue: Map<Constructor, IObject> = new Map();
export class ValidationUtility {
  private static _ajvJsonSchema = schemaKeyValue;
  public static get ajvJsonSchema() {
    return this._ajvJsonSchema;
  }

  public static setRequiredProperties(ctorName: Constructor, arg: IObject) {
    this._addSchemaIfNotExist(ctorName);

    const schemaObject = <IObject>this._ajvJsonSchema.get(ctorName);
    merge(schemaObject, arg);
    if (schemaObject.required) {
      delete schemaObject.required;
    }
  }

  public static setRequired(ctorName: Constructor, requires: string) {
    this._addSchemaIfNotExist(ctorName);
    this._addReservedKeywordIfNotExist('required', {required: []}, ctorName);

    const {required} = <IObject>this._ajvJsonSchema.get(ctorName);
    if (!required.includes(requires)) {
      required.push(requires);
    }
  }

  public static setRequiredDependencies(ctorName: Constructor, requires: IObject) {
    // keyword "dependencies" deprecated in draft 2019-09
    // keyword "dependentRequired" & "dependentSchemas" NEW in draft 2019-09
    this._addSchemaIfNotExist(ctorName);
    this._addReservedKeywordIfNotExist('dependencies', {dependencies: requires}, ctorName);

    const {dependencies: deps} = <IObject>this._ajvJsonSchema.get(ctorName);
    for (const key in requires) {
      merge(deps, {[key]: requires[key]});
    }
  }

  public static setProperties(ctorName: Constructor, properties: IObject) {
    this._addSchemaIfNotExist(ctorName);
    this._addReservedKeywordIfNotExist('type', {type: 'object'}, ctorName);
    this._addReservedKeywordIfNotExist('properties', {properties}, ctorName);

    const {properties: props} = <IObject>this._ajvJsonSchema.get(ctorName);
    for (const key in properties) {
      merge(props, {[key]: properties[key]});
    }
  }

  public static setMessage(ctorName: Constructor, path: string, errorMessage: string) {
    const schemaObject = <IObject>this._ajvJsonSchema.get(ctorName);
    JsonPointer.set(schemaObject, path, errorMessage, true);
  }

  private static _addReservedKeywordIfNotExist(prop: string, obj: Object, ctorName: Constructor) {
    if (this._ajvJsonSchema.has(ctorName)) {
      const schemaObject = <IObject>this._ajvJsonSchema.get(ctorName);
      if (!schemaObject[prop]) {
        Object.assign(schemaObject, obj);
      }
    }
  }

  private static _addSchemaIfNotExist(ctorName: Constructor) {
    if (!this._ajvJsonSchema.has(ctorName)) {
      this._ajvJsonSchema.set(ctorName, {});
    }
  }
}
