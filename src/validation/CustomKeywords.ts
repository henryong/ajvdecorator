import Ajv, {_, KeywordCxt, Code} from 'ajv';

const customKeywords = function (ajv: Ajv) {
  ajv.addKeyword({
    keyword: 'lowercase',
    type: 'string',
    schemaType: 'boolean',
    code(cxt: KeywordCxt) {
      const {data, schema} = cxt;
      const op = schema ? _`!==` : _`===`;
      const validation: Code = _`${data}.toLowerCase()`;

      cxt.fail(_`${data} ${op} ${validation}`);
    },
    error: {message: 'should contain only lower case characters'}
  });
  ajv.addKeyword({
    keyword: 'uppercase',
    type: 'string',
    schemaType: 'boolean',
    code(cxt: KeywordCxt) {
      const {data, schema} = cxt;
      const op = schema ? _`!==` : _`===`;
      const validation: Code = _`${data}.toUpperCase()`;

      cxt.fail(_`${data} ${op} ${validation}`);
    },
    error: {message: 'should contain only upper case characters'}
  });
};

export default customKeywords;
