import Ajv from 'ajv';

const customFormats = function (ajv: Ajv) {
  ajv.addFormat('password', /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})/);
  ajv.addFormat('alphanumerical', /^(\w*\d+\w+)|(\w+\d+\w*)$/);
  ajv.addFormat('alphabetical', /^([a-zA-Z ]+)$/);
};

export default customFormats;
