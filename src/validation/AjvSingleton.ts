import Ajv from 'ajv';
import ajvErrors from 'ajv-errors';
import addFormat from 'ajv-formats';
import addkeywords from 'ajv-keywords';
import addCustomFormats from './CustomFormats';
import addCustomKeywords from './CustomKeywords';

import {IValidatorOptions} from '../common/types';

const ajvOptions: IValidatorOptions = {
  code: {source: true, optimize: 5},
  allErrors: true
};

/**
 * Singleton class for (initialising) AJV single instance
 * @export
 * @class AjvSingleton
 */
export class AjvSingleton {
  private static _ajvInstance: Ajv;
  /**
   * Private constructor prevents creation of new instance of class
   * as purposes of Singleton class is to obtain an instance of a class.
   * @private
   * @constructor
   * @memberof AjvSingleton
   */
  private constructor() {}
  /**
   * @public
   * @readonly
   * @static
   * @param {IAjvOptions} [options]
   * @return {Ajv}
   * @memberof AjvSingleton
   */
  public static getInstance(options?: IValidatorOptions): Ajv {
    if (!AjvSingleton._ajvInstance) {
      const ajv = new Ajv(Object.assign(options ?? {}, ajvOptions));

      AjvSingleton._ajvInstance = ajv;

      ajvErrors(ajv);
      addFormat(ajv);
      addkeywords(ajv);
      addCustomFormats(ajv);
      addCustomKeywords(ajv);
    }
    return AjvSingleton._ajvInstance;
  }
}
