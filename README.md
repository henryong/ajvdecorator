[travis-image]: https://api.travis-ci.org/nestjs/nest.svg?branch=master
[travis-url]: https://travis-ci.org/nestjs/nest
[linux-image]: https://img.shields.io/travis/nestjs/nest/master.svg?label=linux
[linux-url]: https://travis-ci.org/nestjs/nest

  <p align="center">
    An alternative to  <a href="https://www.npmjs.com/package/class-validator" target="blank">Class-Validator</a> TS decorator pattern validation library. <br/>
    Internally uses <a href="https://www.npmjs.com/package/ajv" target="blank">AJV</a> library to do JSON Schema Validations.
  </p>
<p align="center">
  <a href="https://www.npmjs.com/~nestjscore"><img src="https://img.shields.io/npm/v/ajv-class-decorator.svg" alt="NPM Version" /></a>
  <a href="https://www.npmjs.com/~nestjscore"><img src="https://img.shields.io/npm/l/ajv-class-decorator.svg" alt="Package License" /></a>
  <a href="https://www.npmjs.com/~nestjscore"><img src="https://img.shields.io/npm/dm/ajv-class-decorator.svg" alt="NPM Downloads" /></a>
  <a href="https://snyk.io/test/npm/ajv-class-decorator"><img src="https://snyk.io/test/npm/ajv-class-decorator/badge.svg" alt="Known Vulnerabilities" data-canonical-src="https://snyk.io/test/npm/ajv-class-decorator"></a>
</p>

## Description

⚠ WARNING: Library is still under regular development ⚠

## Installation

```bash
$ npm i ajv-class-decorator
```

## Examples

---

### 🧩 Create the validation schema. This is usually the Data Transfer Object.

```typescript
// creating interface is OPTIONAL
export interface ICreateCatDto {
  age: number;
  legs: number;
  email?: string;
  password: string;
  name: string;
  ipv4address: string;
}

enum Emails {
  'haha@email.com',
  'hehe@email.com'
}

@AllRequired()
@AdditionalProperties(false)
export class CreateCatDTO implements ICreateCatDto {
  @Minimum(1)
  @IsInteger32()
  public age!: number;

  @Maximum(4)
  @Minimum(1)
  public legs!: number;

  @DependentRequired(['password'])
  @Enum(Emails)
  @IsEmail()
  public email?: string;

  @IsStrongPassword()
  public password!: string;

  // 2nd param is Object with 'message' key for custom message
  @IsAlphabetic({message: 'this is a message'})
  @IsLowercase()
  public name!: string;

  @IsIPv4()
  public ipv4address!: string;
}
```

### 🧩 You may choose to initialize the Validator with certain params.

```typescript
Validator.init({
  coerceTypes: true,
  removeAdditional: true,
  ownProperties: true,
  generateCode: true,
  outDir: `${path.resolve()}/validators`
});
```

### 🧩 Get the validation results of `IValidationResults`.

```typescript
const newCat = new CreateCatDTO();
newCat.age = 1;
newCat.legs = 4;
newCat.email = 'haha1@email.com';
newCat.password = 'P@$$w0rd';
newCat.name = 'hello1';
newCat.ipv4address = '192.168.1.1';

// Optionally use Object.assign() for assigning properties to Class for validation.
// But you'll lose runtime checks for additional properties that don't exist in the Class / Object.
Object.assign(newCat, {hehe: '!@#$%^&*()_+'});
```

---

## Features:

✅ on-the-fly property validation\
✅ Modulerise each validation schema\
✅ Add custom formats\
✅ Add custom keywords\
✅ Minify to compress generated validation code\
✅ Allow adding of directory path for storing generated file to\
✔️ ❗️Add tests❗️\
✔️ Benchmarks\
✔️ Allow async validation\
✔️ Custom error message (WIP)\
✔️ Continue to add more validation features (WIP)\
✔️ Moving window validations - e.g. future date, past date
